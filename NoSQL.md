> "You joined a new project. The project is going through some performance and scaling issues. After some analysis, the team lead asks you to investigate possibly using a NoSQL database will improve performance
Investigate at least 5 NoSQL databases and why they are used"

# NoSQL Databases
In our daily life, we have noticed that there are thousands of tweets and photos on Instagram are uploaded in one second. To handle this huge amount of data, we need a distributed database system that runs multiple nodes and partitions. So, in case of any node crashes for any reason, the system should continue to work.

Let's discuss a few NoSQL databases and its uses:

##### 1. MongoDB
It is mainly used when we are expecting a lot of reads and write operations from our application but we do not care much about some of the data being lost in the server crash.
For example, we can store the required dataset in the MongoDB database used for behavior analysis.

##### 2. Cassandra
It is mainly used when we are expecting more writing operations than reading operations. It is used in situations where we need more availability than consistency. 
For example, we can use the Cassandra database for social networking sites but cannot use it for banking purposes.

##### 3. ElasticSearch
It is mainly used when we require a full-text search. It can be easily analyzed, store, and search for huge volumes of data.
For example, when we type something there are high chances of spelling mistakes. So we use some in-built fuzzy matchings like Grammarly extension in webpages. The Grammarly extension uses the ElasticSearch database to store this huge data. 

##### 4. Amazon DynamoDB
It is mainly used when we are looking for a database that can handle simple key-value queries but those queries are very large in number.
For example, online ticket booking where the data needs to be highly consistent.

##### 5. HBase
It is used when we require random and real-time access to the data and want to easily store real-time messages for billions of messages. It will be meaningless if our data volume is small.


### References
1. https://javarevisited.blogspot.com/2019/03/top-5-nosql-database-web-developers-should-learn.html#axzz6eoO9SIsd
2. https://www.couchbase.com/resources/why-nosql
3. https://www.analyticsvidhya.com/blog/2020/09/different-nosql-databases-every-data-scientist-must-know/
